﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fade : MonoBehaviour {

	public GameObject BlackScreen;
	public GameObject DeScreen;
	private static bool FadeIn = false;
	private static bool FadeOut = false;

	Func<bool> FOF = () => FadeOut == false;
	Func<bool> FIF = () => FadeIn == false;


	void Start () {
		
	}
	

	void Update () {
		if (FadeIn) {
			FadeThisIn ();
		} else if (FadeOut) {
			FadeThisOut ();
		}
	}

	void FadeThisIn(){
		Color co = BlackScreen.GetComponent<Image> ().color;
		co.a = Mathf.Min (co.a + 0.01f, 1);
		BlackScreen.GetComponent<Image> ().color = co;

		if (co.a >= 1) {
			FadeIn = false;
		}
	}
	void FadeThisOut(){
		Color co = BlackScreen.GetComponent<Image> ().color;
		co.a = Mathf.Max (co.a - 0.01f, 0);
		BlackScreen.GetComponent<Image> ().color = co;

		if (co.a <= 0) {
			FadeOut = false;
		}
	}

	private IEnumerator FadeThis(GameObject activate, GameObject deactivate){
		FadeIn = true;
		yield return new WaitUntil (FIF);
		//activate.SetActive (true);
		deactivate.SetActive (false);
		//FadeOut = true;
		//yield return new WaitUntil (FOF);
	}

	public void StartFade(){
		Color co = BlackScreen.GetComponent<Image> ().color;
		co.a = 0;
		BlackScreen.GetComponent<Image> ().color = co;
		BlackScreen.SetActive (true);
		StartCoroutine (FadeThis (null, DeScreen));
	}
}
