﻿using UnityEngine.Audio;

public class ApplicationValues {

	static public string FileName;
	static public int EarthHealth = 10;
	static public bool isHard = false;
	static public float Score = 0;
	static public float HighScore = 0;
	static public int Part = 1;
	static public bool demo = true;
	static public int FreeContinue = 0;
	static public AudioMixer GameMixer;
	static public bool FirstPlay = true;
}
